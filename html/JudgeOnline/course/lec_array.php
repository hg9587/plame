﻿
<html>
   <head>
      <title>배열</title>
	<?php require_once("lec_header.php"); ?>
   </head>
   <body>
       <!--보충 설명 테스트용 버튼입니다-->
       <!--<button onclick="toggleSupply();">누르면 보충설명이 생기거나 사라집니다</button><br>-->
       <!--보충 설명 테스트용 버튼입니다-->
   <h3>배열</h3>
    
<?php require_once("floating.php"); ?>
<p><h4>1. 배열 개요</h4>
배열이란 무엇일까요? 쉽게 설명하면 '아파트'입니다.<br>
배열은 예제로 나가도록 하겠습니다. 예제의 상황 설정은 다음과 같습니다.<br>
예제의 상황 설정 : 아파트에 사는 사람들의 인원 수를 저장하고 싶다.<br>
구조 : 3층, 4호짜리 아파트 (101호 ~ 304호까지)<br>
소스 코드 :<br>


<pre class="brush:c">
#include &lt;stdio.h&gt;
int main(void) {
  int floor101, floor102, floor103, floor104;
  int floor201, floor202, floor203, floor204;
  int floor301, floor302, floor303, floor304;
  /*.......
  */
}
</pre>

... 이러한 소스 만들기 귀찮죠?<br>
괜히 시간낭비 하는 것 같고.. 하는 중에도 '이건 아닌데..' 생각이 들 것 같아요.<br>
그래서 착한 개발자님들께서, "아 그냥 문자 하나로 편하게 만들어~~"<br>
이렇게 나온 것이 "배열"이라는 놈입니다.<br>
 <br>
배열의 선언을 위 예제를 변형시켜서 만들어 보면 다음과 같습니다.<br>

<pre class="brush:c">
#include &lt;stdio.h&gt;

int main(void) {
  int floor[3][4];
  /*....*/
}
</pre>
<div class="supply">
  <p>
  주의해야 할 것은 컴퓨터는 숫자를 '0'부터 셉니다.<br>
  즉, [3][4]의 배열이 있다면 컴퓨터는 다음과 같이 받아들입니다.<br>
([0][0]) ([0][1]) ([0][2]) ([0][3])<br>
([1][0]) ([1][1]) ([1][2]) ([1][3])<br>
([2][0]) ([2][1]) ([2][2]) ([2][3])<br>
그러니까 floor[0][0]이 101호를 가리키게 되고, floor[2][3]이 304호를 가리키게 되는 것이죠.<br>
</p>
</div>

<h4>2. 1차원 배열</h4>
우리는 위에서 조금 복잡한걸 배웠어요. 왜? 위에꺼는 2차원 배열이니까. (그게 뭔지는 이따가 배웁니다.)<br>
1차원 배열은 다음과 같이 선언합니다.<br><br>
<strong>자료형 배열이름 [숫자];</strong>
<br><br>
변수와 마찬가지로 자료형에 따라 배열이 저장할 수 있는 자료의 종류가 달라집니다.<br>
변수와 다른 점은 []안에 배열의 길이 즉, 변수의 개수를 설정하는 부분입니다.<br>
"어? 괄호가 하나네요?" 예. 1차원은 괄호가 하나입니다.<br>
1차원은 선이죠? 그래서 다음과 같습니다. 이러한 함수를 만들어봅시다. int floor[3];<br>
<img src="./img/arr1.png" width="431px" height="160px" />
<br>
우선 12칸의 메모리에는 floor이라는 이름이 붙습니다.<br>
다음에 각각 4칸씩 floor[0], floor[1], floor[2]라는 이름이 붙지요.<br>
<div class="supply">
(4칸인 이유 : 한 칸은 1btye를 의미, int는 4byte 자료형이기 때문)<br>
</div>

 이번엔 배열에 초기값을 부여해 보겠습니다. 중괄호를 사용합니다.<br>
int a[5]={2,4,6,8,10};<br>
배열에 차례대로 들어 갑니다.<br>
<div class="supply"> 이 경우 배열의 선언과 동시에 초기값을 넣어야 합니다.<br>
int a[5]; a={2,4,6,8,10}; 이렇게 따로 작성하면 컴퓨터는 이해하지 못해요! 그 이유는 심화강의에서 <br>알려드리겠습니다.<br>
</div>
이 배열 값을 그대로 화면에 출력 해 볼까요?<br>

<pre class="brush:c">
#include &lt;stdio.h&gt;
int main() {
  int a[5] = {2, 4, 6, 8, 10};
  printf("%d %d %d %d %d\n", a[0], a[1], a[2], a[3], a[4]);
}
</pre>
3개의 값을 키보드로 받아 이를 그대로 화면에 출력 해 볼까요?<br>

<pre class="brush:c">
#include &lt;stdio.h&gt;
int main() {
  int a[3];
  scanf("%d", &a[0]);
  scanf("%d", &a[1]);
  scanf("%d", &a[2]);
  printf("%d %d %d", a[0], a[1], a[2]);
}
</pre>
5 개의 원소를 받고 합을 출력하는 경우는 굳이 배열을 사용할 필요가 없습니다.<br>
그러면 배열은 언제 사용하는 게 좋을 까요?<br>
배열은 입력 받은 후 입력 받은 데이터를 다시 사용할 필요가 있는 경우에 사용합니다.<br>

<h3>3. 다차원 배열</h3>
<br>
 바로 그림으로 들어가죠.<br>
<img src="./img/arr2.png" width="584" height="271" /><br><br>
자, 이게 2차원 배열입니다.<br><br>
3차원 배열은 큐브모양같은 것을 생각하면 되겠죠?<br><br>
크기가 5인 1차원 배열이 3개 필요할 경우 이렇게 정의할 수 있습니다.<br><br>
     int a[5],b[5],c[5];<br><br>
그러나 만약 크기가 100인 1차원 배열이 100개가 필요할 경우,<br><br>
     int a[100],b[100],c[100],d[100],e[100],f[100], ...<br><br>
이걸 하려면 하룻밤을 지새워야 할 수 있을 것입니다.<br><br>
그래 놓고 사용할 때 변수 이름이 헷갈려서 또 위로 올라가서 100개를 훑어보겠죠.<br><br>
그런 것을 방지하기 위해서 만들어진 것이 '다차원 배열'입니다.<br><br>
다차원 배열은 2부터 컴퓨터의 메모리가 견딜 수 있는 한계의 차원까지 정의할 수 있습니다.<br><br>
전의 내용에서 1차원 배열을 어떻게 선언하는지 배웠습니다.<br><br>
     자료형 배열이름 [숫자];<br><br>
그렇다면 2차원 배열을 어떻게 선언하는지 알아보겠습니다.<br><br>
     자료형 배열이름 [숫자1][숫자2];<br><br>
이렇게 하면 배열의 크기가 가로 숫자2, 세로 숫자1이 됩니다.<br><br>
이제 배열에 초기값을 지정해 보겠습니다.<br><br>
1차원 배열에서는 배열에 초기값을 지정할 때 중괄호 하나만 넣으면 되었습니다.<br><br>
     int a[5]={2,4,6,8,10};<br><br>
과 같이 말이지요.<br><br>
이와 같이 2차원 배열에서는 배열의 초기값을 지정할 때 중괄호 둘을 넣으면 됩니다.<br><br>
그렇다면 어떻게 중괄호 2개를 넣을까요?<br><br>
2차원 배열은 1차원 배열이 모여서 만들어졌습니다.<br><br>
이 원리를 이용해서 중괄호 안에 중괄호를 넣는 방법으로 선언하면 되겠지요.<br><br>
     int a[5][5]={{1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7},{4,5,6,7,8},{5,6,7,8,9}};<br><br>

     <h3>4. 정리</h3>

 원래 배열이라는 것이 실제 저렇게 선으로, 박스 모양으로, 큐브 모양으로 생성되는 것은 아닙니다.<br><br>
이해를 위해서 저렇게 그림으로 표현한 것 뿐입니다.<br><br>
실제로 배열이 어떻게 생성되는가는 심화에서 보도록 하고, 짜투리 문제를 풀면서 복습해 보세요.<br><br>

<br>
짜투리 문제 1.<br><br>
이 코드의 결과값은 무엇일까요? <br>

<pre class="brush:c">
#include<stdio.h>
int main()
{
int arr[4]={1,2,3,4};
printf("%d",arr[1]);
  }
</pre>
<br>
</pre>
<button onclick="i();">보기 1: 1</button><br>
<button onclick="c();">보기 2: 2</button><br>
<br>

<div class="supply">
배열의 첫 원소는 arr[1]이 아닌 arr[0]부터 시작합니다. 그러니까 arr[1]은 배열의 첫 원소가 아닌,<br> 두 번째 원소를 가리키겠죠? <br>
</div>
<br>
<br>
짜투리 문제 2.<br><br>
이 코드의 결과값은 무엇일까요? <br>

<pre class="brush:c">
#include <stdio.h>
int main()
{
int arr[4]={1,2,3,4};
printf("%d",arr[arr[0]]);
  }
</pre>
<br>
</pre>
<button onclick="i();">보기 1: error 발생</button><br>
<button onclick="c();">보기 2: 2</button><br>
<br>

<div class="supply">
arr[숫자] 속 숫자에는 상수 뿐 아니라 숫자를 담고 있는 변수도 들어갈 수 있습니다.<br>
우선 arr[0]은 1이고, 그렇다면 arr[arr[0]]은 arr[1]과 같아서 2가 출력되겠죠.
 <br>
</div>
<br>
<br>
짜투리 문제 3.<br><br>
이 코드의 결과값은 무엇일까요? <br>

<pre class="brush:c">
#include <stdio.h>
int main()
{
int arr[4];
arr={1,2,3,4};
printf("%d",arr[4]);
  }
</pre>
<br>
</pre>
<button onclick="i();">보기 1: 4</button><br>
<button onclick="i();">보기 2: 5</button><br>
<button onclick="c();">보기 3: 알 수 없다.</button><br>
<br>

<div class="supply">
배열의 첫 원소가 arr[0]으로 시작하니, 마지막 원소도 arr[4]가 아니라 arr[3]입니다.<br>그럼 arr[4]에는 무엇이 있을까요? 알 수 없습니다.
 <br>
</div>
<br>

</p>
<hr>
<a href='/judgeonline/showmessage.php?pname=nested_for '>[질/답]</a>
<div align=right>
<a href="/html/JudgeOnline/index.php">[홈으로]</a>&nbsp;&nbsp;<a href="javascript:history.go(-1)">[뒤 로]</a>
<a href="/html/JudgeOnline/contest.php?cid=1003">[문제풀기]</a>
</div>

