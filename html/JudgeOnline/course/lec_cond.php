﻿
<html>
   <head>
      <title>조건문</title>
           	<?php require_once("lec_header.php"); ?> 
   </head>
   <body>
       <!--보충 설명 테스트용 버튼입니다-->
       <!--<button onclick="toggleSupply();">누르면 보충설명이 생기거나 사라집니다</button><br>-->

       <!--보충 설명 테스트용 버튼입니다-->
   <h3>조건문</h3>
	<?php require_once("floating.php"); ?>       
<p><h4>1. 조건문 개요</h4>
<br>
 여러분들이 C언어를 배우시는 목적이 무엇인가요? 프로그래밍에 고수가 되어 유명한<br>
  게임을 만들기 위해서? 아니면 혹시 해킹??<br>
<br>
 C언어를 배우시는 목적은 여러 가지가 있을 텐데요. 여기서 만약 내가 C언어를<br>
  배운다면, 해킹을 할 것이다. 라는 생각을 한다고 해봅시다. 만약 뒤에 나오는 문장이 <br>
  조건이고, 뒤에 나오는 문장은 뒤의 조건절의 참 거짓에 영향을 받는 본문입니다. <br>
  이렇게 조건과, 조건의 참 거짓에 따라 달라지는 본문이 C언어의 조건문입니다.<br>
<br>
<br>
<h4>2. if</h4>
 대표적인 조건문은 IF절이 있습니다.<br>
<br><br>
 IF문은 어떤 해당 조건에 참이면 명령어를 실행을 하고, 거짓이면 실행 하지 않습니다. <br>
 그렇기 때문에 여러분들이 IF문을 이해하려면 "만약에~" 라고 이해하시면 되겠습니다.<br>
<br>
다음 이어지는 블럭 조립 코딩 알고리즘을 보면서 이해를 한번 해 보도록 하겠습니다 <br>
<script>
var current=0;
function clk(index){
	var prev = document.getElementById("b"+current);
	prev.setAttribute("class","btn");
	current = index;
	var cur = document.getElementById("b"+current);
	cur.setAttribute("class","btn  btn-warning");
	
}
</script>
<a class="btn  btn-warning" href="2_13.html" target="frm" id="b0" onclick="clk(0);">0</a>
<a class="btn" href="2_14.html" target="frm" id="b1" onclick="clk(1);">1</a>
<a class="btn" href="2_15.html" target="frm" id="b2" onclick="clk(2);">2</a>
<a class="btn" href="2_19.html" target="frm" id="b3" onclick="clk(3);">3</a>
<iframe src="2_13.html" width="100%" height="500px" border="0" scrolling="no" name="frm"></iframe>




다음은 IF문의 기본 구조입니다.<br>

<pre class="brush:c">
/*if 제어문 : 조건에 따라 실행함 */
if(조건) {/*참인지 아닌지 확인함 */
  명령어;/* 조건이 참일 때 실행 */
}
</pre>
잘 이해하셨나요? 여러분들이 넣는 값에 마다 실행할지 안할지 흐름을 제어하는 <br>
조건문으로 사용할 수 있습니다. 만일 조건문이 항상 참이거나 항상 거짓이면 결과는 <br>
동일합니다. 간단한 문제를 풀어보겠습니다.<br>
<br>
<h5>짜투리 문제</h5><br>
다음 코드의 결과는?
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  if(1){
  printf(“조건이 참입니다.\n”);
}
</pre>
<button onclick="i();">보기 1: 출력 값이 없다.</button><br>
<button onclick="c();">보기 2: 조건이 참입니다.</button><br>
<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  if(0){
  printf(“조건이 거짓입니다..\n”);
}
</pre>
<button onclick="c();">보기 1: 출력 값이 없다.</button><br>
<button onclick="i();">보기 2: 조건이 거짓입니다.</button><br>
<br>
그렇다면 다음엔 조건이 참일 경우와 거짓일 경우에는 어떻게 되는지를 살펴보겠습니다. <br>
<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a=5;
  if(a>4){
  printf(“a는 4보다 크다..\n”);
}
</pre>
<img src="./img/cond1.bmp" width="400" height="160" /><br>
IF문의 조건이 참일 때 <br>
<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a=3;
  if(a>4){
  printf(“a는 4보다 크다..\n”);
}
</pre>
<img src="./img/cond2.bmp" width="400" height="160" /><br>
IF문의 조건이 거짓일 때<br>
그러면 조건은 그냥 이런식으로 미리 변수에 입력해서 대조하나요? 아닙니다. 당연히 <br>
표준입력함수를 써서 여러분들이 입력한 값에 따라 조건이 참인지 거짓인지 다르게 할 <br>수 
있습니다.<br>
<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a
  scanf(“%d”,&a);;
  if(a>4){
  printf(“a는 4보다 크다..\n”);
}
</pre>

<div class="supply"><br>
(scanf라는 함수는 변수가 아닌 변수의 주소 값을 받기 때문에 &를 꼭 붙이셔야 합니다.)<br>
</div><br>

<img src="./img/cond3.PNG" width="379" height="152" /><br>
입력된 값이 4보다 클 때와 작을 때 각각의 결과 값<br>
<br>
<img src="./img/cond4.bmp" width="266" height="374" /><br>
<br>
 순서도로 나타낼때는 다이아몬드 모양이 해당 조건에 따라 판단을 하는 화살표를 사용합니다.<br>
<br>



<h4>3. IF else</h4><br>
 IF절은 조건문이 참일 경우 명령어를 실행하고, 조건문이 거짓일 경우 명령어를 무시하고 넘어갑니다. 조건문이 거짓일 경우 <br>명령어를 실행하는 방법이 있다면 유용하겠죠? 그 방법은 IF 절 다음으로 else절을 IF절과 동일한 문법으로 작성하는 것입니다.<br>
 다음은 if else의 기본 구조입니다.<br>
<br>
<img src="./img/cond5.bmp" width="201" height="303" /><br>
<br>
 if절을 한번더 사용하면 되는 거 아닌가요? 뭐 하러 else를 사용하나요? 라고 생각하실 수 있습니다. 다음과 같은 경우를 보시면 <br>이해하실 것입니다.
<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a;
  printf(“당신의 점수는?”);
  scanf(“%d”,&a);
  if(a>=60){
    printf(“합격했습니다.\n”);
  }if(a<60){
    printf(“불합격했습니다.\n”);
  }
}
</pre>
<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a;
  printf(“당신의 점수는?”);
  scanf(“%d”,&a);
  if(a>=60){
    printf(“합격했습니다.\n”);
  }else{
    printf(“불합격했습니다.\n”);
  }
}
</pre>
<br>
두 코드의 차이점은 if(조건문)과 else를 각각 써서 나타내었는데요. 그냥 IF문만 <br>
사용하면 조건을 써야하지만, 해당 조건이 거짓일 때의 실행하는 경우라면 저런식으로 <br>
번거롭게 쓰기 보다 거짓일 경우에는 그냥 else를 쓰는게 편합니다.<br>
 <br>
 다음은 두 코드의 flow chart입니다.<br>
<img src="./img/cond6.PNG" width="372" height="366" /><br>
<br>
else를 사용한 코드의 flow chart가 좀 더 짧은 것을 확인할 수 있습니다.<br>
<br>
<div class="supply"><br>
(else 절을 사용한다면 참과 거짓이 한 번에 결정되기 때문에 불필요한 loop를 통과할<br>
 필요가 없어서 flow가 더 짧아집니다. 좋아집니다. else를 사용하려면 먼저 IF를 <br>
 사용해야 하며 else를 쓸 때 앞에서 IF문을 쓸 때처럼 중괄호를 안 써도 되지만, <br>
 웬만해서 중괄호를 사용해주세요. )<br>
</div><br>
<br>
<h5>짜투리 문제</h5><br>
그럼 다음 코드들의 결과 값을 한번 맞추어 볼까요?<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a = 4;
  if(a==4){
    printf(“a는 4입니다.\n”);
  }
  if(a==5){
    printf(“a는 5입니다.\n”);
  }
}
</pre><br>
<button onclick="c();">보기 1 : a는 4입니다.</button><br>
<button onclick="i();">보기 2: a는 5입니다.</button>
<br>

<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a = 5;
  if(a==4){
    printf(“a는 4입니다.”);
  } else{
    printf(“a는 4가 아닙니다.”)
}
  if(a==5){
    printf(“ a는 5입니다.”);
  }
}
</pre><br>
<button onclick="i();">보기 1: a는 4입니다. a는 5입니다.</button><br>
<button onclick="c();">보기 2: a는 4가 아닙니다. a는 5입니다.</button><br>

 또한 앞서서 여러분들이 거짓일 때의 경우는 논리 부정으로 사용했지만, else를 사용해도 됩니다.<br>
<br>
 <pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a = 3;
  if(!(a>4)){
    printf(“ a는 4보다 크지 않다.”);
}
</pre><br>
<br>
 이 논리 부정의 코드는 else절로써 치환이 가능합니다.<br>

<pre class="brush:c"><br>
#include&lt;stdio.h>
main(){
  int a = 3;
  if(a>4)){}
  else  {
    printf(“ a는 4보다 크지 않다.”);
}
</pre>
<br>
<h4>4. switch case</h4><br>
switch문이란, 조건문의 일종인데, 여러 개의 if~else 문을 대신하여 간결하게 작성할 때<br>
 사용하는 것입니다. if~else 문이 중첩되어 있으면 가독성이 떨어지기 때문에 <br>
 스위치문이 필요합니다.<br>
<br>
그러나 switch문 다음의 괄호()에는 "i <= 0" 이런 식의 판단문이 들어갈 수는 없고,<br>
 정수형이나 문자형(char)의 숫자만 들어갈 수 있는 제약이 있습니다. double 등의 <br>
 실수는 안되고 error C2450: switch expression of type 'double' is illegal 이런 <br>
 에러가 납니다. switch는 "함수"가 아니고 "키워드"입니다.<br>
<br>
<pre class="brush:c">
switch (정수형 변수) {
  case 상수 : 실행문; break;
  case 상수 : 실행문; break;
  case 상수 : 실행문; break;
  case 상수 : 실행문; break;

  default : 실행문; break;
}
</pre>
대표적인 switch 구문입니다. switch 문에서 주의해야 할 점은 각 case문 끝에 break;<br>
 를 꼭 붙여야 한다는 것입니다. break; 가 없으면, 그 아래쪽의 case문들까지 모두 <br>
 실행되어 버립니다. break;를 만날 때까지 멈추지 않고 계속 실행됩니다.<br>
<br>
<img src="./img/loop2.bmp" width="567" height="368" /><br>사진부분입니다<br>
<br>




<hr>
<a>[질/답]</a>
<div align=right>
<a href="/html/JudgeOnline/index.php">[홈으로]</a>&nbsp;&nbsp;<a href="/html/JudgeOnline/lecture.php">[뒤 로]</a>
<a href="/html/JudgeOnline/contest.php?cid=1005">[문제풀기]</a>
</div>

<br>
