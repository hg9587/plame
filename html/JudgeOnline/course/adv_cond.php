﻿
<html>
   <head>
      <title>조건문 - 심화/응용</title>
      	<?php require_once("lec_header.php"); ?> 
   </head>
   <body>
       <!--보충 설명 테스트용 버튼입니다-->
       <!--<button onclick="toggleSupply();">누르면 보충설명이 생기거나 사라집니다</button><br>-->

       <!--보충 설명 테스트용 버튼입니다-->
   <h3>조건문 - 심화/응용</h3>
             <?php require_once("floating.php"); ?>
<p><h4>조건문 심화 응용</h4>

<h4>1. Else if</h4><br>
 else IF는 앞의 IF문이 아닐 때 만약에~ 이 조건이 맞으면 이라는 것입니다. 그런<br>
  경우에는 경우의 수가 3가지 이상이 된다면 참과 거짓으로만 충분하지 않기 때문에<br>
   조건문 안에 조건문을 넣어야 하는 상황을 만들게 됩니다.<br>
<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a;
  pritnf("당신은 몇 학년 입니까?\n");
  scanf("%d", &a);
  if(a == 1){
    printf("1학년 이군요!\n");
  }else{
    if(a == 2){
      printf("2학년 이군요!\n");
    }else{
      if(a == 3){
        printf("3학년 이군요!\n");
      }
    }
  }
}
</pre>

</p>
 IF else문과 IF문을 가지고 중첩한다면 이런 식으로 코드를 쓸 수 있지만 중괄호의<br>
 위치와 코드를 이해하기가 상당히 보기가 좋지 않게 작성이 됩니다.<br>
<br>
<br>
 그래서 다음과 같이 작성 시 좀 더 보기가 간결해 집니다.<br>
<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a;
  pritnf("당신은 몇 학년 입니까?\n");
  scanf("%d", &a);
  if(a == 1){
    printf("1학년 이군요!\n");
  }else if(a == 2){
    printf("2학년 이군요!\n");
  }else if(a == 3){
    printf("3학년 이군요!\n");
  } 
}
</pre><br>
 조건문 안에 조건문을 중첩하게 됨  else에 IF를 붙여서 보기가 간결해 졌습니다.<br>
<br>
 하지만 이 예시는, 초등학교 4학년 같이 모든 조건이 맞지 않을 때는 아무것도 출력이 <br>
 되지 않습니다. 그럴 때에는 모든 것을 포괄할 수 있는 조건을 마지막에 기입하는 <br>
 방법이 있습니다.<br>
<br>
<pre class="brush:c">
#include&lt;stdio.h>
main(){
  int a;
  pritnf("당신은 몇 학년 입니까?\n");
  scanf("%d", &a);
  if(a == 1){
    printf("1학년 이군요!\n");
  }else if(a == 2){
    printf("2학년 이군요!\n");
  }else if(a == 3){
    printf("3학년 이군요!\n");
  }else{
    printf("초등학생이거나 대학생인가요?\n");
  } 
}
</pre>
<br>
 이렇게 하면 모든 경우에 대해 출력 값을 가지게 됩니다.<br><br><br><br><br>

앞선 기본과정에서 switch문에서 각각의 case 다음엔 꼭 break; 라는 문구를 적어야 한다는 것 기억하시나요?<br>
 그것에 대해 좀 더 자세히 알아볼께요.<br>

다음 사진은 case 다음 break;를 쓰지 않았을 때의 flow chart예요.<br>

<img src="./img/loop2.bmp" width="567" height="368" /><br>

 flow chart에서 확인할 수 있는 것처럼 처음 case로 들어간 다음엔 그 다음 case들을 꼭 거쳐야지<br>
 switch 구문이 완료되는 것을 알 수 있어요.<br>
 만약 조건에 의해 case a1이 선택되었다면 맨 마지막 default 값까지 선택 되고서야 끝나겠죠?<br>
 이렇게 되면 switch 구문의 장점이 사라져요. 꼭 break를 쓰셔야해요.<br>





 다음은 break;를 썼을 경우의 flow chart예요.<br>

<img src="./img/loop2.bmp" width="567" height="368" /><br>

 어떠한 경우라도 각 case를 한번만 거치고 switch 구문이 끝나는 것을 알 수 있겠죠?<br>
 이렇게 break 구문은 정말 중요하답니다. 가끔 출력 조건에 따라선 상위 case를 모두 출력하는 것이 필요할 때가 있어요.<br>
 예를 들면 각 case가 50, 60, 70, 80, 90, 100일 때 70이상을 모두 출력하시오. 라는 문제가 나왔을 땐 그러하겠죠?<br>
 이렇게 상황에 맞는 문법들이 중요해요.



<br>

<div class="supply">
보충부분<br>
<h5>짜투리 문제입니다 </h5>
<button onclick="i();">Wrong Answer</button><br>
<button onclick="c();">Correct Answer</button><br>


</div>
<img src="./img/loop2.bmp" width="567" height="368" /><br>사진부분입니다



</p>





<hr>
<a>[질/답]</a>
<div align=right>
<a href="/html/JudgeOnline/index.php">[홈으로]</a>&nbsp;&nbsp;<a href="javascript:history.go(-1)">[뒤 로]</a>
<a href="/html/JudgeOnline/contest.php?cid=1006">[문제풀기]</a>
</div>

