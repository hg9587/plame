<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $view_title?></title>
	<link rel=stylesheet href='./template/<?php echo $OJ_TEMPLATE?>/<?php echo isset($OJ_CSS)?$OJ_CSS:"hoj.css" ?>' type='text/css'>
	<script language="javascript" type="text/javascript" src="include/jquery-latest.js"></script>
    <script language="javascript" type="text/javascript" src="include/jquery.flot.js"></script>
</head>
<body>
<div id="wrapper">
	<?php require_once("oj-header.php");?>
<div id=main>
	<center>
	
	<h2> This page is for the lecture</h2>
	<br>
	<table border="1" cellpadding="35px">
	<tr> <td> <a href="lec_loop.php">반복문</a></td>
		 <td> <a href="lec_cond.php">조건문</a></td>
		 <td> <a href="lec_array.php">배열</a></td></tr>
		 
	<tr><td><a href="adv_loop.php">반복문 심화</a></td> <td><a href="adv_cond.php">조건문 심화</a></td> <td><a href="adv_array.php">배열 심화</a></td></tr>
		 </table>
	</center>


	<br><br><br><br>

<div id=foot>

<?php require_once("oj-footer.php"); ?>
</div><!--end foot-->
</div><!--end main-->
</div><!--end wrapper-->


</body>
</html>
