<?php
	//oj-header.php
	$MSG_FAQ="자주 묻는 질문";
	$MSG_BBS="댓글달기";
	$MSG_HOME="처음 화면";
	$MSG_PROBLEMS="문제 모음";
	$MSG_STATUS="채점 상황 열람";
	$MSG_RANKLIST="순위";
	$MSG_CONTEST="대회";
	$MSG_RECENT_CONTEST="Recent";
	
	$MSG_LOGOUT="로그아웃";
	$MSG_LOGIN="로그인";
	$MSG_REGISTER="가입하기";
	$MSG_ADMIN="관리자";
	$MSG_STANDING="순위";
	$MSG_STATISTICS="통계";
	$MSG_USERINFO="정보 수정";
	$MSG_MAIL="이메일";
	
	//status.php
	$MSG_Pending="제출중";
	$MSG_Pending_Rejudging="재채점 제출중";
	$MSG_Compiling="컴파일중";
	$MSG_Running_Judging="실행 & 채점중";
	$MSG_Accepted="정답";
	$MSG_Presentation_Error="형식오류";
	$MSG_Wrong_Answer="오답";
	$MSG_Time_Limit_Exceed="시간 초과";
	$MSG_Memory_Limit_Exceed="메모리 사용 초과";
	$MSG_Output_Limit_Exceed="출력 초과";
	$MSG_Runtime_Error="런타임 에러";
	$MSG_TEST_RUN="채점 완료";//test runing done
	$MSG_Compile_Error="컴파일 에러";
	$MSG_Compile_OK="정상 컴파일";
	$MSG_RUNID="제출 번호";
	$MSG_USER="사용자";
	$MSG_PROBLEM="문제 번호";
	$MSG_RESULT="결과";
	$MSG_MEMORY="메모리 사용량";
	$MSG_TIME="수행 시간";
	$MSG_LANG="사용언어";
	$MSG_CODE_LENGTH="코드 길이";
	$MSG_SUBMIT_TIME="제출 시간";
	//problemstatistics.php
	$MSG_PD="대기";
	$MSG_PR="재평가 대기";
	$MSG_CI="컴파일";
	$MSG_RJ="실행&평가";
	$MSG_AC="정답";
	$MSG_PE="형식오류";
	$MSG_WA="오답";
	$MSG_TLE="시간초과";
	$MSG_MLE="메모리초과";
	$MSG_OLE="출력 오버런";
	$MSG_RE="런타임에러";
	$MSG_CE="컴파일에러";
	$MSG_CO="컴파일성공";
	$MSG_TR="TEST";
	
	//problemset.php
	$MSG_SEARCH="Search";
	$MSG_PROBLEM_ID="문제 ID";
	$MSG_TITLE="문제명";
	$MSG_SOURCE="출처";
	$MSG_SUBMIT="제출";
	
	//ranklist.php
	$MSG_Number="순위";
	$MSG_NICK="닉네임";
	$MSG_SOVLED="해결 문제 수";
	$MSG_RATIO="정답률";
	
	//registerpage.php
	$MSG_USER_ID="사용자 ID";
	$MSG_PASSWORD="비밀번호";
	$MSG_REPEAT_PASSWORD="비밀번호 반복 입력";
	$MSG_SCHOOL="소속";
	$MSG_EMAIL="이메일";
	$MSG_REG_INFO="등록 정보";
	$MSG_VCODE="인증코드";

	//problem.php
	$MSG_NO_SUCH_PROBLEM="해당 문제를 열람할 수 없습니다!";
	$MSG_Description="문제 설명"  ;
	$MSG_Input="입력"  ;
	$MSG_Output= "출력" ;
	$MSG_Sample_Input= "입력 예시" ;
	$MSG_Sample_Output= "출력 예시" ;
	$MSG_HINT= "도움말" ;
	$MSG_Source= "출처" ;
	$MSG_Time_Limit="시간 제한";
	$MSG_Memory_Limit="메모리 제한";
	
	//admin menu
	$MSG_SEEOJ="SeeOJ";
	$MSG_ADD="추가하기 ";
	$MSG_LIST="목록";
	$MSG_NEWS="News";
	$MSG_TEAMGENERATOR="TeamGenerator";
	$MSG_SETMESSAGE="SetMessage";
	$MSG_SETPASSWORD="비밀번호 변경";
	$MSG_REJUDGE="Rejudge";
	$MSG_PRIVILEGE="관리자 권한 부여";
	$MSG_GIVESOURCE="GiveSource";
	$MSG_IMPORT="Import";
	$MSG_EXPORT="Export";
	$MSG_UPDATE_DATABASE="UpdateDatabase";
	$MSG_ONLINE="Online";

	//someone please translate these
	$MSG_PRIVATE_WARNING="This is a private contest which you don't have privilege。";
	$MSG_WATCH_RANK="Click HERE to watch contest rank.";
	$MSG_Public="Public";
	$MSG_Private="Private";
	$MSG_Running="Running";
	$MSG_Start="Start";
	$MSG_TotalTime="Total";
	$MSG_LeftTime="Left";
	$MSG_Ended="Finished";
	
?>
